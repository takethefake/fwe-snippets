import React, { useState } from "react";

export const App = () => {
  let [count, setCount] = useState(0);
  const onButtonClickHandler = () => {
    console.log("clicked");
    // use a function if your next state depends on the current state
    // provide it with a value otherwise
    setCount((count) => {
      console.log("current Value", count);
      return count + 1;
    });
  };
  return (
    <div className="container">
      <p>You have clicked the button {count} times</p>
      <button onClick={onButtonClickHandler}>Click Me!</button>
    </div>
  );
};
