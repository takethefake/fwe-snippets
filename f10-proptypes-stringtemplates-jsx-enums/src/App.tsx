import React from "react";

enum MessageType {
  INFO = "info",
  ERROR = "error",
  NONE = "",
}

const Message: React.FC<{
  type?: MessageType;
}> = ({ children, type = MessageType.NONE }) => {
  return <div className={`message ${type}`}>{children}</div>;
};

export const App = () => (
  <div className="container">
    <Message type={MessageType.INFO}>Hello World</Message>
  </div>
);
