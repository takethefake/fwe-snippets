import React from "react";

export const App = () => {
  const onButtonClickHandler = () => {
    console.log("clicked");
  };

  function onInputChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    console.log(e.target.value);
  }
  return (
    <div className="container">
      <button onClick={onButtonClickHandler}>Click Me!</button>
      <input type="text" onChange={onInputChangeHandler} />
    </div>
  );
};
