import React from "react";
import { Message, MessageType } from "./components/Message/Message";
import "./styles.css";

export const App = () => (
  <div className="container">
    <Message type={MessageType.INFO}>Hello World</Message>
    <div className="user info">Hallo Nutzer</div>
  </div>
);
