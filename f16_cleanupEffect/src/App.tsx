import React, { useState, useEffect } from "react";

export const App = () => {
  const [showUseEffect, setShowUseEffect] = useState(false);
  return (
    <>
      <button
        onClick={() => {
          setShowUseEffect(!showUseEffect);
        }}
      >
        Show Hide UseEffect Component
      </button>
      {showUseEffect && <UseEffect />}
    </>
  );
};

export const UseEffect = () => {
  const [siteTitle, setSiteTitle] = useState("hallo");
  useEffect(() => {
    console.log("useEffect");
    window.document.title = siteTitle;
    return () => {
      window.document.title = "unmounted";
    };
  }, [siteTitle]);
  return (
    <div className="container">
      <input
        onChange={(e) => {
          setSiteTitle(e.currentTarget.value);
        }}
      ></input>
    </div>
  );
};
