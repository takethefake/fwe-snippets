import React from "react";
import { StyledMessage, MessageType } from "./components/Message/Message";
import "./styles.css";

export const App = () => (
  <div className="container">
    <StyledMessage type={MessageType.ERROR}>Hello World</StyledMessage>
    <div className="user info">Hallo Nutzer</div>
  </div>
);
