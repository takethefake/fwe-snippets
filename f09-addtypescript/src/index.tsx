import React from "react";
import ReactDOM from "react-dom";

const rootElement = document.getElementById("root");
const element = <div className="container">Hello World</div>;
ReactDOM.render(element, rootElement);
