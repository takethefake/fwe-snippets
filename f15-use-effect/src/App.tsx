import React, { useState, useEffect } from "react";

export const App = () => {
  const [siteTitle, setSiteTitle] = useState("hallo");
  useEffect(() => {
    console.log("useEffect");
    window.document.title = siteTitle;
  }, [siteTitle]);
  console.log("before Render");
  return (
    <div className="container">
      <input
        onChange={(e) => {
          setSiteTitle(e.currentTarget.value);
        }}
      ></input>
    </div>
  );
};
