import React, { useState, useEffect } from "react";

export const App = () => {
  const [showUseEffect, setShowUseEffect] = useState(false);
  return (
    <>
      <button
        onClick={() => {
          setShowUseEffect(!showUseEffect);
        }}
      >
        Show Hide UseEffect Component
      </button>
      {showUseEffect && <UseEffect></UseEffect>}
    </>
  );
};

function useDocumentTitle(initialTitle: string) {
  const [siteTitle, setSiteTitle] = useState(initialTitle);
  useEffect(() => {
    console.log("useEffect");
    window.document.title = siteTitle;
    return () => {
      window.document.title = "unmounted";
    };
  }, [siteTitle]);
  return { setSiteTitle };
}

export const UseEffect = () => {
  const { setSiteTitle } = useDocumentTitle("hallo");
  const [counter, setCounter] = useState(0);
  return (
    <div className="container">
      <input
        onChange={(e) => {
          setSiteTitle(e.currentTarget.value);
        }}
      ></input>
      <button
        onClick={() => {
          setCounter(counter + 1);
        }}
      >
        {counter}
      </button>
    </div>
  );
};
