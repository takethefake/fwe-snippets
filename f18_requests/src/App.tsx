import React, { useState, useEffect } from "react";

export interface JokeResponse {
  type: string;
  value: Value;
}

export interface Value {
  id: number;
  joke: string;
  categories: any[];
}

export const App = () => {
  const [joke, setJoke] = useState<JokeResponse | null>(null);
  const fetchJoke = async () => {
    const jokeRequest = await fetch("http://api.icndb.com/jokes/random");
    const jokeJson = (await jokeRequest.json()) as JokeResponse;
    console.log(jokeJson);
    setJoke(jokeJson);
  };
  useEffect(() => {
    fetchJoke();
  }, []);
  return (
    <>
      <div className="container">{joke !== null ? joke.value.joke : ""}</div>
      <button
        onClick={() => {
          fetchJoke();
        }}
      >
        Fetch Joke
      </button>
    </>
  );
};
